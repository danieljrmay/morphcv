<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:morphcv="http://danieljrmay.com/morphcv"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:date="http://exslt.org/dates-and-times"
		xmlns:str="http://exslt.org/strings"
                extension-element-prefixes="date str"
		exclude-result-prefixes="xhtml"
		version="1.0">
  
  <xsl:output 
      method="text" 
      encoding="utf-8"
      media-type="application/x-latex"/>

  <xsl:param name="theme" select="'classic'"/>
  
  <xsl:strip-space elements="*"/>
  
  <xsl:template match="//*[@disabled='true']">
    <!-- Replace any disabled element (and its children) with a comment -->
    <xsl:text>&#x0a;% Disabled </xsl:text>
    <xsl:value-of select="name(.)"/>
    <xsl:text> element removed&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="text()">
    <!--<xsl:text>X</xsl:text>-->
    <xsl:call-template name="escape-for-latex">
      <xsl:with-param name="text" select="."/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="escape-for-latex">
    <xsl:param name="text"/>
    <xsl:variable name="replaced-dollar" select="str:replace($text,'$','&#x5c;$')"/>
    <xsl:variable name="replaced-ampersand" select="str:replace($replaced-dollar,'&#x26;','&#x5c;&#x26;')"/>
    <xsl:variable name="replaced-underscore" select="str:replace($replaced-ampersand,'_','\_')"/>
    <xsl:variable name="replaced-latex" select="str:replace($replaced-underscore,'LaTeX','\LaTeX')"/>
    <xsl:variable name="replaced-thinspace" select="str:replace($replaced-latex,' ','\,')"/>
    <xsl:value-of disable-output-escaping="yes" select="$replaced-thinspace"/>
  </xsl:template>

  
  <xsl:template match="morphcv:morphcv">
    <xsl:variable name="display-name" select="morphcv:personal-information/morphcv:personal-name/morphcv:display-name"/>
    <xsl:variable name="mobile-phone-url" select="morphcv:personal-information/morphcv:telephone-numbers/morphcv:telephone-number[@type='mobile']/@url"/>
    <xsl:variable name="mobile-phone" select="morphcv:personal-information/morphcv:telephone-numbers/morphcv:telephone-number[@type='mobile']"/>
    <xsl:variable name="email-url" select="morphcv:personal-information/morphcv:emails/morphcv:email/@url"/>
    <xsl:variable name="email" select="morphcv:personal-information/morphcv:emails/morphcv:email"/>
    <xsl:variable name="website-url" select="morphcv:personal-information/morphcv:websites/morphcv:website/@url"/>
    <xsl:variable name="website" select="morphcv:personal-information/morphcv:websites/morphcv:website"/>
    <xsl:text>\documentclass[a4paper]{morphcv-</xsl:text><xsl:value-of select="$theme"/><xsl:text>}&#x0a;</xsl:text>
    <xsl:text>\hypersetup{&#x0a;&#x09;pdfauthor={</xsl:text>
    <xsl:value-of select="$display-name"/>
    <xsl:text>},&#x0a;&#x09;pdftitle={</xsl:text>
    <xsl:value-of select="$display-name"/>
    <xsl:text> — Curriculum Vitæ},&#x0a;&#x09;pdfsubject={Curriculum Vitæ},&#x0a;&#x09;pdfkeywords={</xsl:text>
    <xsl:value-of select="$display-name"/>
    <xsl:text>, Curriculum Vitæ, Curriculum Vitae, CV, Résumé, Resume},</xsl:text>
    <xsl:text>&#x0a;&#x09;pdfproducer={morphcv-to-pdf},&#x0a;&#x09;pdfcreator={morphcv},</xsl:text>
    <xsl:text>&#x0a;&#x09;colorlinks=true,&#x0a;&#x09;allcolors={blue}&#x0a;}&#x0a;</xsl:text>
    <xsl:text>\begin{document}&#x0a;</xsl:text>
    <xsl:text>\name{</xsl:text><xsl:value-of select="$display-name"/><xsl:text>}&#x0a;</xsl:text>
    <xsl:if test="not(morphcv:profile/morphcv:role/@disabled = 'false')">
      <xsl:text>\role{</xsl:text><xsl:value-of select="morphcv:profile/morphcv:role"/><xsl:text>}&#x0a;</xsl:text>
    </xsl:if>
    <xsl:text>\contact{</xsl:text>
    <xsl:if test="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:flat">      
      <xsl:value-of select="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:flat"/>
      <xsl:text>, </xsl:text>
    </xsl:if>
    <xsl:value-of select="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:street"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:city"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:postal-code"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:country"/>
    <xsl:text>.}&#x0a;&#x09;{\href{</xsl:text>
    <xsl:value-of select="$mobile-phone-url"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="$mobile-phone"/>
    <xsl:text>}}&#x0a;&#x09;{\href{</xsl:text>
    <xsl:value-of select="$email-url"/>
    <xsl:text>}{\texttt{</xsl:text>
    <xsl:value-of select="$email"/>
    <xsl:text>}}}&#x0a;&#x09;{\href{</xsl:text>
    <xsl:value-of select="$website-url"/>
    <xsl:text>}{\texttt{</xsl:text>
    <xsl:value-of select="$website"/>    
    <xsl:text>}}}&#x0a;</xsl:text>
    <!--    <xsl:apply-templates/> -->
    <!-- TODO web address goes here -->
    <xsl:apply-templates select="morphcv:profile"/>
    <xsl:apply-templates select="morphcv:skills-list"/>
    <xsl:apply-templates select="morphcv:work-experience-list"/>
    <xsl:apply-templates select="morphcv:education-list"/>
    <xsl:apply-templates select="morphcv:achievements-list"/>
    <xsl:text>\end{document}</xsl:text>
  </xsl:template>

  <xsl:template match="morphcv:email">
    <xsl:text>\href{mailto:</xsl:text>
    <xsl:value-of select="@url"/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>    
  </xsl:template>
  
  <xsl:template match="morphcv:profile">
    <xsl:text>&#x0a;&#x0a;\section{Profile}&#x0a;</xsl:text>
    <xsl:apply-templates select="morphcv:statement/*"/>
  </xsl:template>

  <xsl:template match="morphcv:skills-list">
    <xsl:text>&#x0a;\section{Skills}&#x0a;</xsl:text>
    <xsl:apply-templates select="morphcv:computer"/>
    <xsl:apply-templates select="morphcv:organisational"/>
  </xsl:template>

  <xsl:template match="morphcv:organisational">
    <xsl:text>&#x0a;\subsection{Organisational}&#x0a;</xsl:text>
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="morphcv:computer">
    <xsl:text>&#x0a;\subsection{Technical}&#x0a;</xsl:text>
    <xsl:text>\begin{description}[style=sameline,nosep]&#x0a;</xsl:text>
    <xsl:apply-templates select="morphcv:technical-skill-list"/>
    <xsl:text>\end{description}&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="morphcv:technical-skill-list">
    <xsl:text>\item[Languages, Libraries \&amp; Frameworks:]</xsl:text>
    <xsl:for-each select="./morphcv:computer-language[not(@disabled = 'true')]|./morphcv:computer-library[not(@disabled = 'true')]|./morphcv:computer-framework[not(@disabled = 'true')]">
      <xsl:apply-templates/>
      <xsl:choose>
	<xsl:when test="position() = last()">
	  <xsl:text>.&#x0a;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>\item[Applications, Servers \&amp; Containers:]</xsl:text>
    <xsl:for-each select="./morphcv:computer-application[not(@disabled = 'true')]|./morphcv:computer-server[not(@disabled = 'true')]">
      <xsl:apply-templates/>
      <xsl:choose>
	<xsl:when test="position() = last()">
	  <xsl:text>.&#x0a;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>\item[Operating Systems, Platforms \&amp; Infrastructure:]</xsl:text>
    <xsl:for-each select="./morphcv:computer-os[not(@disabled =
                          'true')]|./morphcv:computer-platform[not(@disabled
                          =
                          'true')]|./morphcv:computer-infrastructure[not(@disabled
                          = 'true')]">
      <xsl:apply-templates/>
      <xsl:choose>
	<xsl:when test="position() = last()">
	  <xsl:text>.&#x0a;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="morphcv:position | morphcv:employer | morphcv:name | morphcv:description">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="morphcv:work-experience-list">
    <xsl:text>&#x0a;&#x0a;\section{Work Experience}&#x0a;</xsl:text>
    <xsl:apply-templates select="morphcv:work-experience"/>
  </xsl:template>
  
  <xsl:template match="morphcv:work-experience">
      <xsl:text>&#x0a;\datedsubsection{</xsl:text>
      <xsl:apply-templates select="morphcv:position"/>
      <xsl:text> @ </xsl:text>
      <xsl:apply-templates select="morphcv:employer"/>
      <xsl:text>}{</xsl:text>
      <xsl:apply-templates select="morphcv:date-list"/>
      <xsl:text>}&#x0a;</xsl:text>
      <xsl:apply-templates select="morphcv:activities/*"/>
  </xsl:template>

  <xsl:template match="morphcv:date-list">
    <xsl:for-each select="./*">
    <xsl:choose>
      <xsl:when test="position() = last()">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="."/>
        <xsl:text>, </xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="morphcv:date-range">
    <xsl:apply-templates select="morphcv:start-date"/>
    <xsl:text>–</xsl:text>
    <xsl:choose>
      <xsl:when test="./morphcv:end-date">
        <xsl:apply-templates select="morphcv:end-date"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Present</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

    <xsl:template match="morphcv:date | morphcv:start-date | morphcv:end-date">
    <xsl:variable name="date-as-string" select="normalize-space(.)"/>
    <xsl:variable name="year" select="date:year($date-as-string)"/>
    <xsl:variable name="month-name" select="date:month-name($date-as-string)"/>
    <xsl:element name="time">
      <xsl:attribute name="datetime"><xsl:value-of select="."/></xsl:attribute>
      <xsl:value-of select="$month-name"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="$year"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="morphcv:education-list">
    <xsl:text>&#x0a;&#x0a;\section{Education}&#x0a;</xsl:text>
    <xsl:text>\begin{itemize}[parsep=0ex, itemsep=0ex, leftmargin=*]&#x0a;</xsl:text>
    <xsl:apply-templates select="morphcv:education"/>
    <xsl:text>\end{itemize}&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="morphcv:education">
    <xsl:text>\item </xsl:text>
    <xsl:call-template name="escape-for-latex">
      <xsl:with-param name="text" select="normalize-space(morphcv:qualification)"/>
    </xsl:call-template>
    <xsl:text>, </xsl:text>
    <xsl:call-template name="escape-for-latex">
      <xsl:with-param name="text" select="normalize-space(morphcv:organisation)"/>
    </xsl:call-template>
    <xsl:text>, </xsl:text>
    <xsl:apply-templates select="morphcv:date-list"/>
  </xsl:template>

  <xsl:template match="morphcv:achievements-list">
    <xsl:text>&#x0a;&#x0a;\section{Achievements}&#x0a;</xsl:text>
    <xsl:apply-templates select="morphcv:awards-list"/>
    <xsl:apply-templates select="morphcv:projects-list"/>
    <xsl:apply-templates select="morphcv:publications-list"/>
    <xsl:apply-templates select="morphcv:references-list"/>    
  </xsl:template>

  <xsl:template match="morphcv:publications-list">
    <xsl:text>&#x0a;\subsection{Publications}&#x0a;</xsl:text>
    <xsl:text>\begin{flushleft}</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{flushleft}</xsl:text>
  </xsl:template>

  <xsl:template match="morphcv:awards-list">
    <xsl:text>&#x0a;\subsection{Awards}&#x0a;</xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="morphcv:projects-list">
    <xsl:text>&#x0a;\subsection{Projects}&#x0a;</xsl:text>
    <xsl:text>\begin{description}[style=sameline,nosep]&#x0a;</xsl:text>
    <xsl:apply-templates select="morphcv:project|morphcv:meta-project"/>
    <xsl:text>\end{description}&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="morphcv:project">
    <xsl:text>\item[\href{</xsl:text>
    <xsl:value-of select="@url"/>
    <xsl:text>}{</xsl:text>
    <xsl:apply-templates select="morphcv:name"/>
    <xsl:text>}</xsl:text>
    <xsl:text>:]&#x0a;</xsl:text>
    <xsl:apply-templates select="morphcv:description"/>
    <xsl:text>&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="morphcv:meta-project">
    <xsl:text>\item[</xsl:text>
    <xsl:apply-templates select="morphcv:name"/>
    <xsl:text>:]&#x0a;</xsl:text>
    <xsl:apply-templates select="morphcv:list"/>
    <xsl:text>&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="morphcv:list">
    <xsl:apply-templates select="morphcv:list-item"/>
  </xsl:template>

  <xsl:template match="morphcv:list-item">
    <xsl:apply-templates/>
    <xsl:choose>
      <xsl:when test="position() = last()">
        <xsl:text>.</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>, </xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="morphcv:references-list">
    <xsl:text>&#x0a;\section{References}&#x0a;</xsl:text>
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="xhtml:p">
    <xsl:apply-templates/>
    <xsl:text>&#x0a;&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:abbr[not(@title)]">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="xhtml:abbr">
    <xsl:text>\pdftooltip{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="normalize-space(@title)"/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:em[not(@title)]|xhtml:i[not(@title)]">
    <xsl:text>\emph{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:em|xhtml:i">
    <xsl:text>
    \pdftooltip{\emph{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}}{</xsl:text>
    <xsl:value-of select="normalize-space(@title)"/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:strong[not(@title)]|xhtml:b[not(@title)]">
    <xsl:text>
    \textbf{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:strong|xhtml:b">
    <xsl:text>\pdftooltip{\textbf{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}}{</xsl:text>
    <xsl:value-of select="normalize-space(@title)"/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:blockquote">
    <xsl:text>&#x0a;\begin{quotation}&#x0a;\emph{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#x0a;\end{quotation}&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:q">
    <xsl:text>\begin{quote}</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{quote}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:code">
    <xsl:text> \texttt{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:a[not(@title)]">
    <xsl:text>\href{</xsl:text>
    <xsl:value-of select="@href"/>
    <xsl:text>}{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:a">
    <xsl:text>\href{</xsl:text>
    <xsl:value-of select="@href"/>
    <xsl:text>}{\pdftooltip{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="normalize-space(@title)"/>
    <xsl:text>}}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:ul">
    <xsl:text>\begin{itemize}[parsep=0ex, itemsep=0ex, leftmargin=*]&#x0a;</xsl:text>
    <xsl:apply-templates select="xhtml:li"/>
    <xsl:text>\end{itemize}&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:ol">
    <xsl:text>\begin{enumerate}&#x0a;</xsl:text>
    <xsl:apply-templates select="xhtml:li"/>
    <xsl:text>\end{enumerate}&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:li">
    <xsl:text>\item </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:dl">
    <xsl:text>\begin{description}[style=sameline,nosep]&#x0a;</xsl:text>
    <xsl:apply-templates select="xhtml:dt | xhtml:dd"/>
    <xsl:text>\end{description}&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:dt">
    <xsl:text>\item[</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:dd">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template name="comma-separated-list">
    <xsl:for-each select="*[not(@disabled = 'true')]">
      <xsl:apply-templates/>
      <xsl:choose>
	<xsl:when test="position() = last()">
	  <xsl:text>.&#x0a;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="comma-separated-list-item">
      <xsl:apply-templates/>
      <xsl:choose>
	<xsl:when test="position() = last()">
	  <xsl:text>.&#x0a;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
