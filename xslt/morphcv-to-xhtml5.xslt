<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:morphcv="http://danieljrmay.com/morphcv"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:date="http://exslt.org/dates-and-times"
                extension-element-prefixes="date"
		exclude-result-prefixes="xhtml"
		version="1.0">
  
  <xsl:output 
      method="xml" 
      encoding="utf-8"
      omit-xml-declaration="yes"
      cdata-section-elements="script style"
      indent="yes"
      media-type="application/xhtml+xml"/>

  <xsl:param name="stand-alone" select="'false'"/>
  <xsl:param name="theme" select="'modern'"/>
  <xsl:param name="stand-alone-css-path" select="concat('morphcv-css-theme-', $theme, '.xml')"/>
  
  <xsl:variable name="display-name" select="/morphcv:morphcv/morphcv:personal-information/morphcv:personal-name/morphcv:display-name"/>

  <xsl:template match="//*[@disabled='true']">
    <xsl:comment>Disabled <xsl:value-of select="name(.)"/> element removed</xsl:comment>
  </xsl:template>

  <xsl:template match="morphcv:morphcv">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
      <head>
	<title>
	  <xsl:value-of select="$display-name"/>
	— Curriculum Vitæ</title>
	<meta charset="utf-8"/>
	<xsl:element name="meta">
	  <xsl:attribute name="name">author</xsl:attribute>
	  <xsl:attribute name="content">
	    <xsl:value-of select="$display-name"/>
	  </xsl:attribute>
	</xsl:element>
	<xsl:element name="meta">
	  <xsl:attribute name="name">description</xsl:attribute>
	  <xsl:attribute name="content">Curriculum vitæ of <xsl:value-of select="$display-name"/>.</xsl:attribute>
	</xsl:element>
	<xsl:element name="meta">
	  <xsl:attribute name="name">generator</xsl:attribute>
	  <xsl:attribute name="content">morphcv</xsl:attribute>
	</xsl:element>
	<xsl:element name="meta">
	  <xsl:attribute name="name">keywords</xsl:attribute>
	  <xsl:attribute name="content">curriculum vitæ, curriculum vitae, CV, resume, <xsl:value-of select="$display-name"/></xsl:attribute>
	</xsl:element>
	<xsl:choose>
	  <xsl:when test="$stand-alone='true'">
	    <style type="text/css">
	      <xsl:value-of select="document('morphcv-css-viewport-navigation.xml')"/>
	      <xsl:value-of select="document($stand-alone-css-path)"/>
	    </style>
	    <script>
	      <xsl:value-of select="document('morphcv-js-viewport-navigation.xml')"/>
	    </script>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:element name="link">
	      <xsl:attribute name="href">css/<xsl:value-of select="$theme"/>.css</xsl:attribute>
	      <xsl:attribute name="rel">stylesheet</xsl:attribute>
	      <xsl:attribute name="title"><xsl:value-of select="$theme"/></xsl:attribute>
	    </xsl:element>
	    <!--<link href="css/fancy.css" rel="alternate stylesheet" title="Fancy"/>-->
	  </xsl:otherwise>
	</xsl:choose>
      </head>
      <body>
	<main id="main">
	  <h1>
	    <xsl:value-of select="$display-name"/>
	  </h1>
          <!--<xsl:apply-templates/>-->
	  <xsl:apply-templates select="morphcv:personal-information"/>
	  <xsl:apply-templates select="morphcv:profile"/>
	  <xsl:apply-templates select="morphcv:skills-list"/>	  
	  <xsl:apply-templates select="morphcv:work-experience-list"/>
	  <xsl:apply-templates select="morphcv:education-list"/>
	  <xsl:apply-templates select="morphcv:achievements-list"/>
          <!--
	  <xsl:apply-templates
          select="morphcv:achievements-list/morphcv:references-list"/>
          -->
	</main>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template match="morphcv:personal-information">
    <address>
      <ul class="very-compact">
	<xsl:apply-templates select="morphcv:postal-addresses/*"/>
	<xsl:apply-templates select="morphcv:telephone-numbers/*"/>
	<xsl:apply-templates select="morphcv:emails/*"/>
	<xsl:apply-templates select="morphcv:websites/*"/>
      </ul>
    </address>
  </xsl:template>

  <xsl:template match="morphcv:postal-address">
    <li class="postal-address" title="Postal address.">
      <xsl:if test="morphcv:flat">
        <xsl:value-of select="morphcv:flat"/>,
      </xsl:if>
      <xsl:value-of select="morphcv:street"/>,
      <xsl:value-of select="morphcv:city"/>,
      <xsl:value-of select="morphcv:postal-code"/>,
      <xsl:value-of select="morphcv:country"/>.
    </li>
  </xsl:template>

  <xsl:template match="morphcv:email">
      <li class="email">
	<xsl:element name="a">
	<xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute>
	<xsl:attribute name="title">Send an email.</xsl:attribute>
	<xsl:value-of select="."/>
	</xsl:element>
      </li>
  </xsl:template>
  
  <xsl:template match="morphcv:telephone-number">
    <xsl:variable name="phone-type" select="@type"/>
    <xsl:choose>
      <xsl:when test="$phone-type='mobile'">
	<li>
	  <xsl:element name="a">
	    <xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute>
	    <xsl:attribute name="title">Call mobile phone.</xsl:attribute>
	    <xsl:value-of select="."/>
	  </xsl:element>
	</li>
      </xsl:when>
      <xsl:otherwise>
	<li>
	  <xsl:element name="a">
	    <xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute>
	    <xsl:attribute name="title">Call landline.</xsl:attribute>
	    <xsl:value-of select="."/>
	  </xsl:element>
	</li>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="morphcv:website">
      <li>
	<xsl:element name="a">
	  <xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute>
	  <xsl:attribute name="title">Visit website.</xsl:attribute>
	  <xsl:value-of select="."/>
	</xsl:element>
      </li>
  </xsl:template>

  <xsl:template match="morphcv:profile">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="morphcv:role">
    <h2><xsl:value-of select="."/></h2>
  </xsl:template>
  
  <xsl:template match="morphcv:statement">
    <section id="profile">
      <h2>Profile</h2>
      <xsl:apply-templates/>
    </section>
  </xsl:template>

  <xsl:template match="morphcv:work-experience-list">
    <section id="work_experience">
      <h2>Work Experience</h2>
      <xsl:apply-templates select="morphcv:work-experience"/>
    </section>
  </xsl:template>
  
  <xsl:template match="morphcv:work-experience">
      <section>
	<h3>
	  <xsl:apply-templates select="morphcv:position"/>
	  <xsl:value-of select="' @ '"/>
	  <xsl:apply-templates select="morphcv:employer"/>
	  <xsl:value-of select="' '"/>
	  <span class="time-period">
            <xsl:apply-templates select="morphcv:date-list"/>
	  </span>
	</h3>
	<xsl:apply-templates select="morphcv:activities/*"/>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:position">
    <span class="position">
      <xsl:value-of select="."/>
    </span>
  </xsl:template>

  <xsl:template match="morphcv:employer">
    <span class="employer">
      <xsl:value-of select="."/>
    </span>
  </xsl:template>

  <xsl:template match="morphcv:date-list">
    <xsl:for-each select="./*">
    <xsl:choose>
      <xsl:when test="position() = last()">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="."/>
        <xsl:text>, </xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="morphcv:date-range">
    <xsl:apply-templates select="morphcv:start-date"/>
    <xsl:text>–</xsl:text>
    <xsl:choose>
      <xsl:when test="./morphcv:end-date">
        <xsl:apply-templates select="morphcv:end-date"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Present</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="morphcv:date | morphcv:start-date | morphcv:end-date">
    <xsl:variable name="date-as-string" select="."/>
    <xsl:variable name="year" select="date:year($date-as-string)"/>
    <xsl:variable name="month-name" select="date:month-name($date-as-string)"/>
    <xsl:element name="time">
      <xsl:attribute name="datetime"><xsl:value-of select="."/></xsl:attribute>
      <xsl:value-of select="$month-name"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="$year"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="morphcv:education-list">
    <section id="education">
      <h2>Education</h2>
      <ul>
	<xsl:apply-templates select="morphcv:education"/>
      </ul>
    </section>
  </xsl:template>

  <xsl:template match="morphcv:education">
    <section>
      <li>
	<xsl:apply-templates select="morphcv:qualification"/>, 
	<xsl:apply-templates select="morphcv:organisation"/>,
        <xsl:apply-templates select="morphcv:date-list"/>.
      </li>
    </section>
  </xsl:template>

  <xsl:template match="morphcv:qualification | morphcv:organisation">
    <xsl:value-of select="."/>
  </xsl:template>
  
  <xsl:template match="morphcv:skills-list">
    <section id="skills">
      <h2>Skills</h2>
      <xsl:apply-templates select="morphcv:computer"/>
      <xsl:apply-templates select="morphcv:organisational"/>
      <!--
      <xsl:apply-templates select="morphcv:communication"/>
      <xsl:apply-templates select="morphcv:linguistic"/>
      <xsl:apply-templates select="morphcv:job"/>
      <xsl:apply-templates select="morphcv:driving"/>
      <xsl:apply-templates select="morphcv:other"/>
      -->
    </section>
  </xsl:template>

  <xsl:template match="morphcv:linguistic">
      <section>
	<h3>Linguistic</h3>
	<xsl:apply-templates/>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:communication">
      <section id="communication">
	<h3>Communication</h3>
	<xsl:apply-templates/>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:organisational">
      <section id="organisational_and_managerial">
	<h3>Organisational &amp; Managerial</h3>
	<xsl:apply-templates/>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:job">
      <section id="jobs">
	<h3>Job</h3>
	<xsl:apply-templates/>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:computer">
      <section id="computer">
	<h3>Technical</h3>
	<dl class="compact">
          <xsl:apply-templates select="morphcv:technical-skill-list"/>
          <!--
	  <xsl:apply-templates select="morphcv:computer-language-list"/>
	  <xsl:apply-templates select="morphcv:computer-library-list"/>
	  <xsl:apply-templates select="morphcv:computer-framework-list"/>
	  <xsl:apply-templates select="morphcv:computer-application-list"/>
	  <xsl:apply-templates select="morphcv:computer-os-list"/>
          -->
	</dl>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:technical-skill-list">
    <dt>Languages, Libraries &amp; Frameworks</dt>
    <dd>
      <xsl:for-each select="morphcv:computer-language[not(@disabled = 'true')] |
                            morphcv:computer-library[not(@disabled = 'true')] |
                            morphcv:computer-framework[not(@disabled = 'true')]">
        <xsl:apply-templates/>
        <xsl:choose>
	  <xsl:when test="position() = last()">
	    <xsl:text>.&#x0a;</xsl:text>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:text>, </xsl:text>
	  </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </dd>
    <dt>Applications, Servers &amp; Containers</dt>
    <dd>
      <xsl:for-each select="morphcv:computer-application[not(@disabled = 'true')] |
                            morphcv:computer-server[not(@disabled = 'true')]">
        <xsl:apply-templates/>
        <xsl:choose>
	  <xsl:when test="position() = last()">
	    <xsl:text>.&#x0a;</xsl:text>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:text>, </xsl:text>
	  </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </dd>
    <dt>Operating Systems, Platforms &amp; Infrastructure</dt>
    <dd>
      <xsl:for-each select="morphcv:computer-os[not(@disabled = 'true')] |
                            morphcv:computer-platform[not(@disabled = 'true')] | 
                            morphcv:computer-infrastructure[not(@disabled = 'true')]">
        <xsl:apply-templates/>
        <xsl:choose>
	  <xsl:when test="position() = last()">
	    <xsl:text>.&#x0a;</xsl:text>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:text>, </xsl:text>
	  </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </dd>
  </xsl:template>

  <xsl:template match="morphcv:driving">
      <section id="driving">
	<h3>Driving</h3>
	<xsl:apply-templates/>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:other">
      <section id="other">
	<h3>Other</h3>
	<xsl:apply-templates/>
      </section>
  </xsl:template>


  <xsl:template match="morphcv:achievements-list">
    <section id="achievements">
      <h2>Achievements</h2>
      <xsl:apply-templates select="morphcv:awards-list"/>
      <xsl:apply-templates select="morphcv:projects-list"/>
      <xsl:apply-templates select="morphcv:publications-list"/>
      <xsl:apply-templates select="morphcv:memberships-list"/>
    </section>
  </xsl:template>

  <xsl:template match="morphcv:publications-list">
      <section id="publications">
	<h3>Publications</h3>
	<xsl:apply-templates/>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:awards-list">
    <section id="awards">
      <h3>Awards</h3>
      <xsl:apply-templates/>
    </section>
  </xsl:template>

  <xsl:template match="morphcv:projects-list">
      <section id="projects">
	<h3>Projects</h3>
	<xsl:apply-templates select="morphcv:project|morphcv:meta-project"/>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:project">
      <section class="project">
	<h4>
	<xsl:element name="a">
	  <xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute>
	  <xsl:value-of select="morphcv:name"/>
	</xsl:element>
	</h4>
	<xsl:apply-templates select="morphcv:description"/>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:meta-project">
      <section class="meta-project">
	<h4>
	<xsl:element name="a">
	  <xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute>
	  <xsl:value-of select="morphcv:name"/>
	</xsl:element>
	</h4>
	<xsl:apply-templates select="morphcv:list"/>
      </section>
  </xsl:template>

  <xsl:template match="morphcv:description">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="morphcv:list">
    <ul>
      <xsl:apply-templates select="morphcv:list-item"/>
    </ul>
  </xsl:template>
  
  <xsl:template match="morphcv:list-item">
    <li>
      <xsl:apply-templates/>
      <xsl:choose>
        <xsl:when test="position() = last()">
          <xsl:text>.</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>, </xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </li>
  </xsl:template>

  <xsl:template match="morphcv:list-item/text()">
    <xsl:value-of select="normalize-space(.)"/>
  </xsl:template>

  <xsl:template match="morphcv:memberships-list">
    <section id="memberships">
      <h3>Memberships</h3>
      <xsl:apply-templates/>
    </section>
  </xsl:template>

  <xsl:template match="morphcv:references-list">
    <section id="references">
      <h2>References</h2>
      <xsl:apply-templates/>
    </section>
  </xsl:template>
  
  <!-- Match any XHTML namespace element -->
  <xsl:template match="xhtml:*">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="@* | node()"/>
    </xsl:element>
  </xsl:template>

  <!-- Match attributes and normalize their whitespace -->
  <xsl:template match="@*">
    <xsl:attribute name="{name()}">
      <xsl:value-of select="normalize-space()"/>
    </xsl:attribute>
  </xsl:template>

  <!-- Not sure what this does! -->
  <xsl:template match="node()">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template name="comma-separated-list">
    <xsl:for-each select="*[not(@disabled = 'true')]">
      <xsl:apply-templates/>
      <xsl:choose>
	<xsl:when test="position() = last()">
	  <xsl:text>.&#x0a;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
