<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:morphcv="http://danieljrmay.com/morphcv"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:date="http://exslt.org/dates-and-times"
		xmlns:str="http://exslt.org/strings"
                extension-element-prefixes="date str"
		exclude-result-prefixes="xhtml"
		version="1.0">
  
  <xsl:output 
      method="text" 
      encoding="utf-8"
      media-type="text/plain"/>

  <xsl:strip-space elements="*"/>

  <xsl:variable name="open-title-block">{\pard \f0\fs48\qc </xsl:variable>
  <xsl:variable name="open-contact-block">{\pard \f0\fs20\qc </xsl:variable>
  <xsl:variable name="open-section-block">{\pard \keepn\f0\fs32\brdrb\brdrs\brdrw10\brsp20\sb100\sa100 </xsl:variable>
  <xsl:variable name="open-subsection-block">{\pard \keepn\f0\fs26\sb75\sa50 </xsl:variable>
  <xsl:variable name="open-subsubsection-block">{\pard \f0\fs20\sb25\sa25 </xsl:variable>
  <xsl:variable name="open-paragraph-block">{\pard \f0\fs20\sb25\sa25 </xsl:variable>
  <xsl:variable name="open-bullet-list-item-block">{\pard \f0\fs20\li300 \'95 </xsl:variable>
  <xsl:variable name="open-ordinal-list-item-block">{\pard \f0\fs20\li300 </xsl:variable>
  <xsl:variable name="open-description-list-item-block">{\pard \f0\fs20\li300 </xsl:variable>
  <xsl:variable name="open-blockquote-block">{\pard \f0\fs20\sb200\sa200\qc </xsl:variable>
  <xsl:variable name="close-block">\par}&#x0a;</xsl:variable>
    
  <xsl:template match="//*[@disabled='true']">
    <!-- Replace any disabled element (and its children) with a comment -->
    <xsl:text>{\comment Disabled </xsl:text>
    <xsl:value-of select="name(.)"/>
    <xsl:text> element removed}&#x0a;</xsl:text>
  </xsl:template>

  <xsl:template match="text()">
    <xsl:value-of select="normalize-space(.)"/>    
  </xsl:template>

  <xsl:template match="morphcv:morphcv">
    <xsl:variable name="display-name" select="morphcv:personal-information/morphcv:personal-name/morphcv:display-name"/>
    <xsl:variable name="mobile-phone-url" select="morphcv:personal-information/morphcv:telephone-numbers/morphcv:telephone-number[@type='mobile']/@url"/>
    <xsl:variable name="mobile-phone" select="morphcv:personal-information/morphcv:telephone-numbers/morphcv:telephone-number[@type='mobile']"/>
    <xsl:variable name="email-url" select="morphcv:personal-information/morphcv:emails/morphcv:email/@url"/>
    <xsl:variable name="email" select="morphcv:personal-information/morphcv:emails/morphcv:email"/>
    <xsl:text>{\rtf1\ansi\deff0&#x0a;</xsl:text>
    <xsl:text>{\fonttbl{\f0\froman Times;}{\f1\fmodern Courier New;}}&#x0a;</xsl:text>
    <xsl:text>{\colortbl&#x0a;</xsl:text>
    <xsl:text>;&#x0a;</xsl:text>
    <xsl:text>\red0\green0\blue0;&#x0a;</xsl:text>
    <xsl:text>\red0\green0\blue255;&#x0a;</xsl:text>
    <xsl:text>}&#x0a;</xsl:text>
    <xsl:text>{\info&#x0a;</xsl:text>
    <xsl:text>{\title </xsl:text><xsl:value-of select="$display-name"/><xsl:text> — Curriculum Vitae}&#x0a;</xsl:text>
    <xsl:text>{\author </xsl:text><xsl:value-of select="$display-name"/><xsl:text>}&#x0a;</xsl:text>
    <!--<xsl:text>{\company My Company Name}&#x0a;</xsl:text>-->
    <!--<xsl:text>{\creatim\yr2004\mo11\dy1\hr8\min34}&#x0a;</xsl:text>-->
    <xsl:text>{\doccomm Created by morphcv}&#x0a;</xsl:text>
    <xsl:text>}&#x0a;</xsl:text>

    <xsl:text>\paperw11909\paperh16834&#x0a;</xsl:text>
    <xsl:text>\margl1200 \margr1200 \margt1000 \margb1000&#x0a;</xsl:text>
    
    <xsl:value-of select="$open-title-block"/>
    <xsl:apply-templates select="morphcv:personal-information/morphcv:personal-name/morphcv:display-name"/>
    <xsl:value-of select="$close-block"/>

    <xsl:if test="not(morphcv:profile/morphcv:role/@disabled = 'true')">
      <xsl:text>\role{</xsl:text><xsl:value-of select="morphcv:profile/morphcv:role"/><xsl:text>}&#x0a;</xsl:text>
    </xsl:if>

    <xsl:value-of select="$open-contact-block"/>
    <xsl:if test="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:flat">
      <xsl:value-of select="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:flat"/>
      <xsl:text>, </xsl:text>
    </xsl:if>
    <xsl:value-of select="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:street"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:city"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:postal-code"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="morphcv:personal-information/morphcv:postal-addresses/morphcv:postal-address/morphcv:country"/>
    <xsl:text>. </xsl:text>
    <xsl:text> \'95  </xsl:text>
    <xsl:text>{\field{\*\fldinst{HYPERLINK "</xsl:text>
    <xsl:value-of select="$mobile-phone-url"/>
    <xsl:text>" }}{\fldrslt{\cf2 </xsl:text>
    <xsl:apply-templates select="$mobile-phone"/>
    <xsl:text>}}}</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:value-of select="$open-contact-block"/>
    <xsl:text>{\field{\*\fldinst{HYPERLINK "</xsl:text>
    <xsl:value-of select="$email-url"/>
    <xsl:text>" }}{\fldrslt{\cf2 </xsl:text>
    <xsl:value-of select="$email"/>
    <xsl:text>}}}</xsl:text>
    <xsl:value-of select="$close-block"/>    
    <xsl:apply-templates select="morphcv:profile"/>
    <xsl:apply-templates select="morphcv:skills-list"/>
    <xsl:apply-templates select="morphcv:work-experience-list"/>
    <xsl:apply-templates select="morphcv:education-list"/>
    <xsl:apply-templates select="morphcv:achievements-list"/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="morphcv:profile">
    <xsl:value-of select="$open-section-block"/>
    <xsl:text>Profile</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates select="morphcv:statement/*"/>
  </xsl:template>

  <xsl:template match="morphcv:skills-list">
    <xsl:value-of select="$open-section-block"/>
    <xsl:text>Skills</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates select="morphcv:computer"/>
    <xsl:apply-templates select="morphcv:organisational"/>
  </xsl:template>

  <xsl:template match="morphcv:organisational">
    <xsl:value-of select="$open-subsection-block"/>
    <xsl:text>Organisational</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="morphcv:computer">
    <xsl:value-of select="$open-subsection-block"/>
    <xsl:text>Technical</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates select="morphcv:technical-skill-list"/>
  </xsl:template>

  <xsl:template match="morphcv:technical-skill-list">
    <xsl:value-of select="$open-description-list-item-block"/>
    <xsl:text>{\b Languages, Libraries &amp; Frameworks: }</xsl:text>
    <xsl:for-each select="morphcv:computer-language[not(@disabled = 'true')] |
                          morphcv:computer-library[not(@disabled = 'true')] |
                          morphcv:computer-framework[not(@disabled = 'true')]">
      <xsl:apply-templates/>
      <xsl:choose>
	<xsl:when test="position() = last()">
	  <xsl:text>.&#x0a;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:value-of select="$close-block"/>
    <xsl:value-of select="$open-description-list-item-block"/>
    <xsl:text>{\b Applications, Servers &amp; Containers: }</xsl:text>
    <xsl:for-each select="morphcv:computer-application[not(@disabled = 'true')] |
                          morphcv:computer-server[not(@disabled = 'true')]">
      <xsl:apply-templates/>
      <xsl:choose>
	<xsl:when test="position() = last()">
	  <xsl:text>.&#x0a;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:value-of select="$close-block"/>
    <xsl:value-of select="$open-description-list-item-block"/>
    <xsl:text>{\b Operating Systems, Platforms &amp; Infrastructure: }</xsl:text>
    <xsl:for-each select="morphcv:computer-os[not(@disabled = 'true')] |
                          morphcv:computer-platform[not(@disabled = 'true')] | 
                          morphcv:computer-infrastructure[not(@disabled = 'true')]">
      <xsl:apply-templates/>
      <xsl:choose>
	<xsl:when test="position() = last()">
	  <xsl:text>.&#x0a;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:value-of select="$close-block"/>
  </xsl:template>
  
  <xsl:template match="morphcv:position | morphcv:employer | morphcv:name | morphcv:description">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="morphcv:work-experience-list">
    <xsl:value-of select="$open-section-block"/>
    <xsl:text>Work Experience</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates select="morphcv:work-experience"/>
  </xsl:template>
  
  <xsl:template match="morphcv:work-experience">
      <xsl:value-of select="$open-subsection-block"/>
      <xsl:apply-templates select="morphcv:position"/>
      <xsl:text> @ </xsl:text>
      <xsl:apply-templates select="morphcv:employer"/>
      <xsl:text> {\fs20 </xsl:text> <!-- TODO -->
      <xsl:apply-templates select="morphcv:date-list"/>
      <xsl:text>}</xsl:text> <!-- TODO -->
      <xsl:value-of select="$close-block"/>
      <xsl:apply-templates select="morphcv:activities/*"/>
  </xsl:template>

  <xsl:template match="morphcv:date-list">
    <xsl:for-each select="./*">
    <xsl:choose>
      <xsl:when test="position() = last()">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="."/>
        <xsl:text>, </xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="morphcv:date-range">
    <xsl:apply-templates select="morphcv:start-date"/>
    <xsl:text>–</xsl:text>
    <xsl:choose>
      <xsl:when test="./morphcv:end-date">
        <xsl:apply-templates select="morphcv:end-date"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Present</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="morphcv:date | morphcv:start-date | morphcv:end-date">
    <xsl:variable name="date-as-string" select="normalize-space(.)"/>
    <xsl:variable name="year" select="date:year($date-as-string)"/>
    <xsl:variable name="month-name" select="date:month-name($date-as-string)"/>
    <xsl:element name="time">
      <xsl:attribute name="datetime"><xsl:value-of select="."/></xsl:attribute>
      <xsl:value-of select="$month-name"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="$year"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="morphcv:education-list">
    <xsl:value-of select="$open-section-block"/>
    <xsl:text>Education</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates select="morphcv:education"/>
  </xsl:template>

  <xsl:template match="morphcv:education">
    <xsl:value-of select="$open-bullet-list-item-block"/>
    <xsl:value-of select="normalize-space(morphcv:qualification)"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="normalize-space(morphcv:organisation)"/>
    <xsl:text>, </xsl:text>
    <xsl:apply-templates select="morphcv:date-list"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="$close-block"/>
  </xsl:template>

  <xsl:template match="morphcv:achievements-list">
    <xsl:value-of select="$open-section-block"/>
    <xsl:text>Achievements</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates select="morphcv:awards-list"/>
    <xsl:apply-templates select="morphcv:projects-list"/>
    <xsl:apply-templates select="morphcv:publications-list"/>
    <xsl:apply-templates select="morphcv:references-list"/>    
  </xsl:template>

  <xsl:template match="morphcv:publications-list">
      <xsl:value-of select="$open-subsection-block"/>
      <xsl:text>Publications</xsl:text>
      <xsl:value-of select="$close-block"/>
      <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="morphcv:awards-list">
    <xsl:value-of select="$open-subsection-block"/>
    <xsl:text>Awards</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="morphcv:projects-list">
    <xsl:value-of select="$open-subsection-block"/>
    <xsl:text>Projects</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates select="morphcv:project"/>
  </xsl:template>

  <xsl:template match="morphcv:project">
    <xsl:value-of select="$open-subsubsection-block"/>
    <xsl:text>{\field{\*\fldinst{HYPERLINK "</xsl:text>
    <xsl:value-of select="@url"/>
    <xsl:text>" }}{\fldrslt{\cf2 </xsl:text>
    <xsl:apply-templates select="morphcv:name"/>
    <xsl:text>}}}</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates select="morphcv:description"/>
  </xsl:template>

  <xsl:template match="morphcv:references-list">
    <xsl:value-of select="$open-section-block"/>
    <xsl:text>References</xsl:text>
    <xsl:value-of select="$close-block"/>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="xhtml:p">
    <xsl:value-of select="$open-paragraph-block"/>
    <xsl:apply-templates/>
    <xsl:value-of select="$close-block"/>
  </xsl:template>

  <xsl:template match="xhtml:em">
    <xsl:text> {\i </xsl:text>
    <xsl:apply-templates/>
    <xsl:text> \i0}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:blockquote">
    <xsl:value-of select="$open-blockquote-block"/>
    <xsl:text>{\i </xsl:text>
    <xsl:apply-templates select="text()"/>    
    <xsl:text>}</xsl:text>
    <xsl:apply-templates select="xhtml:cite"/>
    <xsl:value-of select="$close-block"/>
  </xsl:template>

  <xsl:template match="xhtml:cite"> <!--TODO-->
    <xsl:text>{\plain\fs20 </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:a">
    <xsl:text>{\field{\*\fldinst{HYPERLINK "</xsl:text>
    <xsl:value-of select="@href"/>
    <xsl:text>"}}{\fldrslt{\cf2 </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}}} </xsl:text>
  </xsl:template>

  <xsl:template match="xhtml:ul">
    <xsl:for-each select="xhtml:li">
      <xsl:value-of select="$open-bullet-list-item-block"/>
      <xsl:apply-templates/>
      <xsl:value-of select="$close-block"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="xhtml:dl">
    <xsl:for-each select="xhtml:dt | xhtml:dd">
      <xsl:value-of select="$open-description-list-item-block"/>
      <xsl:choose>
	<xsl:when test="position() = first()">
	  <xsl:text>{\b </xsl:text>
	  <xsl:apply-templates/>
	  <xsl:text>:}</xsl:text>
	</xsl:when>
	<xsl:when test="position() = last()">	  
	  <xsl:apply-templates/>
	  <xsl:text>.&#x0a;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:apply-templates/>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="$close-block"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="comma-separated-list">
    <xsl:for-each select="*[not(@disabled = 'true')]">
	<xsl:apply-templates/>
	<xsl:choose>
	  <xsl:when test="position() = last()">
	    <xsl:text>.&#x0a;</xsl:text>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:text>, </xsl:text>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
