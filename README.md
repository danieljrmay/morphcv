<!-- markdownlint-disable MD033 -->
# <img src="doc/images/logo.png" width="100" alt="morphcv logo"/> morphcv

[![pipeline status](https://gitlab.com/danieljrmay/morphcv/badges/master/pipeline.svg)](https://gitlab.com/danieljrmay/morphcv/commits/master)
[![Copr build
status](https://copr.fedorainfracloud.org/coprs/danieljrmay/morphcv/package/morphcv/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/danieljrmay/morphcv/package/morphcv/)

Generate your CV in multiple formats from a single XML source.
