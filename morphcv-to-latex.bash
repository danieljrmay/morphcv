#!/usr/bin/bash
# morphcv-to-latex.bash
#
# Author: Daniel J. R. May <daniel.may@kada-media.com>
# Version: 0.3, 10 September 2020
# Description: Convert a  morphcv XML to LaTeX.
#
# This file is part of morphcv.
# 
# morphcv is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# morphcv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with morphcv.  If not, see <http://www.gnu.org/licenses/>.

readonly input_path=$1

output_path="$(basename "$input_path" .xml).tex"
readonly output_path

theme=classic
#theme=modern

readonly morphcv_latex_xslt_path_local=xslt/morphcv-to-latex.xslt
readonly morphcv_latex_xslt_path_global=/usr/share/morphcv/xslt/morphcv-to-latex.xslt

function print_usage {
    gettext -s 'Usage: morphcv-to-latex FILE'
    gettext -s 'Where FILE is something like: path/to/my-morphcv-file.xml'
}

if [[ -z "$input_path" ]]
then
    gettext -s 'Syntax error: you must specify a morphcv XML file to be converted to LaTeX.'
    print_usage
    exit 1
fi

if [[ ! -e "$input_path" ]]
then
    gettext -s 'File error: the file you specified does not exist, please check your spelling.'
    print_usage
    exit 2
fi

if [[ ! -r "$input_path" ]]
then
    gettext -s 'File error: the file you specified can not be read, please check its file permissions.'
    print_usage
    exit 3
fi

if (! command -v morphcv-validate > /dev/null )
then
    gettext -s 'Requirements error: unable to find the morphcv-validate command which is needed by this program.'
    gettext -s 'morphcv-validate is part of the morphcv package.'
    exit 4
fi

if (! command -v xsltproc > /dev/null )
then
    gettext -s 'Requirements error: unable to find the xsltproc command which is needed by this program.'
    gettext -s 'xsltproc is part of libxslt, see http://xmlsoft.org/XSLT for more details.'
    gettext -s 'You can typically install it with something like:'
    gettext -s -e "\tdnf install libxslt"
    exit 4
fi

if [[ -r $morphcv_latex_xslt_path_local ]]
then
    morphcv_latex_xslt_path=$morphcv_latex_xslt_path_local
elif [[ -r $morphcv_latex_xslt_path_global ]]
then
    morphcv_latex_xslt_path=$morphcv_latex_xslt_path_global
else
    gettext -s "Requirements error: unable to find morphcv-to-latex.xslt which is needed by this program."
    gettext -s "The file is not present in the following default locations:"
    gettext -s "$morphcv_latex_xslt_path_local"
    gettext -s "$morphcv_latex_xslt_path_global"
    gettext -s "Please check your installation and fix this problem before running this program again."
    exit 6
fi

gettext -s "Checking input is valid morphcv XML..."
if morphcv-validate "$input_path"
then
    # Input validates, so we continue
    :
else
    gettext -s "ERROR: $input_path is invalid morphcv XML. Check the above error messages, fix the issues and try again."
    exit 7
fi

gettext -s "Converting $input_path to LaTeX..."
xsltproc --stringparam theme "$theme" --output "$output_path" "$morphcv_latex_xslt_path" "$input_path"
