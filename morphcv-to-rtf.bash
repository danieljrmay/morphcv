#!/usr/bin/bash
# morphcv-to-rtf.bash
#
# Author: Daniel J. R. May <daniel.may@kada-media.com>
# Version: 0.2, 10 September 2020
# Description: Convert a morphcv XML file to RTF.
#
# This file is part of morphcv.
# 
# morphcv is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# morphcv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with morphcv.  If not, see <http://www.gnu.org/licenses/>.

readonly input_path=$1

intermediate_path="$(basename "$input_path" .xml).rtf.utf8"
readonly intermediate_path

output_path="$(basename "$input_path" .xml).rtf"
readonly output_path

readonly morphcv_rtf_xslt_path_local=xslt/morphcv-to-rtf.xslt
readonly morphcv_rtf_xslt_path_global=/usr/share/morphcv/xslt/morphcv-to-rtf.xslt

function print_usage {
    gettext -s 'Usage: morphcv-to-rtf FILE'
    gettext -s 'Where FILE is something like: path/to/my-morphcv-file.xml'
}

if [[ -z "$input_path" ]]
then
    gettext -s 'Syntax error: you must specify a morphcv XML file to be converted to RTF.'
    print_usage
    exit 1
fi

if [[ ! -e "$input_path" ]]
then
    gettext -s 'File error: the file you specified does not exist, please check your spelling.'
    print_usage
    exit 2
fi

if [[ ! -r "$input_path" ]]
then
    gettext -s 'File error: the file you specified can not be read, please check its file permissions.'
    print_usage
    exit 3
fi

if (! command -v morphcv-validate > /dev/null )
then
    gettext -s 'Requirements error: unable to find the morphcv-validate command which is needed by this program.'
    gettext -s 'morphcv-validate is part of the morphcv package.'
    exit 4
fi

if (! command -v xsltproc > /dev/null )
then
    gettext -s 'Requirements error: unable to find the xsltproc command which is needed by this program.'
    gettext -s 'xsltproc is part of libxslt, see http://xmlsoft.org/XSLT for more details.'
    gettext -s 'You can typically install it with something like:'
    gettext -s -e "\tdnf install libxslt"
    exit 5
fi

if [[ -r $morphcv_rtf_xslt_path_local ]]
then
    morphcv_rtf_xslt_path=$morphcv_rtf_xslt_path_local
elif [[ -r $morphcv_rtf_xslt_path_global ]]
then
    morphcv_rtf_xslt_path=$morphcv_rtf_xslt_path_global
else
    gettext -s "Requirements error: unable to find morphcv-to-rtf.xslt which is needed by this program."
    gettext -s "The file is not present in the following default locations:"
    gettext -s "$morphcv_rtf_xslt_path_local"
    gettext -s "$morphcv_rtf_xslt_path_global"
    gettext -s "Please check your installation and fix this problem before running this program again."
    exit 6
fi

# We convert the morphcv XML file to a rtf.utf8 file which is an RTF
# file but with all UTF-8 characters still in place. This is then
# converted to a genuine RTF file where the UTF-8 are correctly
# escaped where possible.
morphcv-validate "$input_path" && \
    xsltproc --output "$intermediate_path" "$morphcv_rtf_xslt_path" "$input_path" && \
    utf2rtf --output "$output_path" "$intermediate_path"
