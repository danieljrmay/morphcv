# morphcv Todos

- [x] Create logo for morphcv
- [x] Normalize whitespace in attribute values
- [ ] Update XHTML and RTF XSLT files to match the LaTeX version.
- [ ] Add title attribute based toottips to more elements, currently
      we just have support for abbr, em and i.
- [ ] Profile `<role disabled="true">…</role>` not disabling output
- [ ] Add command line switches to executables e.g. output filename
- [ ] Replace disabled="true|false" attribute with disabled="true|all|latex|rtf|xhtml5"?
- [ ] Tidy up bash executables, they could do with more functions and
      better command line argument parsing
- [ ] Add technologies used to projects schema
- [ ] Create classic and contempoary LaTeX styles
  - [ ] Update bash scripts so that they accept a theme style
        parameter when using LaTeX.
- [ ] Add viewport-navigation to XHTML transformation
- [ ] Make XHTML transformation standalone and non-standalone
- [ ] Update contact macro in morphcv.cls and xslt file with website URLs
- [ ] Improve Makefile: Particularly the precheck, check and installcheck tasks.
- [ ] Requirements: texlive-collection-latexrecommended
- [ ] Create native XML to docx conversion
- [ ] RTF and docx missing subject and keywords metadata: This does
      not seem to be appearing in libreoffice when I open the files.
