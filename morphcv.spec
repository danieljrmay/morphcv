Name:           morphcv
Version:        0.3
Release:        1%{?dist}
Summary:        Create a CV in various formats from a single XML source file.

License:        GPLv3
URL:            https://gitlab.com/danieljrmay/%{name}
Source0:        %{url}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  make
Requires:       libreoffice-core libxml2 libxslt texlive-collection-latexrecommended utf2rtf

%description
Create a single morphcv XML file according to the morphcv XML Schema
and then use the simple command line tools to output your CV in
various formats. Current ouput formats include: XHTML5, RTF, LaTeX,
PDF and Europass XML.

%prep
%autosetup

%build
%make_build

%install
%make_install

%post
texhash

%files
%{_bindir}/%{name}-*
%{_datadir}/doc/%{name}
%{_datadir}/%{name}
%{_datadir}/texlive/texmf-dist/tex/latex/%{name}
%doc README.md
%license LICENSE

%changelog
* Fri Feb 18 2022 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.3-1
- Add make to BuildRequires

* Wed Nov 18 2020 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.2-1
- Various theme fixes.
- Fix shebangs.

* Wed Jan 30 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.1-1
- Initial release.
