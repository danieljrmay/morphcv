#!/usr/bin/make -f
#
# morphcv GNU Makefile
#
# See: https://www.gnu.org/software/make/manual
#
# Copyright (c) 2019 Daniel J. R. May
#

# Makefile command variables
DNF=/usr/bin/dnf
DNF_COPR=$(DNF) --assumeyes copr
DNF_INSTALL=$(DNF) --assumeyes install
GIT=/usr/bin/git
GZIP=/usr/bin/gzip
INSTALL=/usr/bin/install
INSTALL_DATA=$(INSTALL) --mode=644 -D
INSTALL_DIR=$(INSTALL) --directory
INSTALL_PROGRAM=$(INSTALL) --mode=755 -D
MOCK=/usr/bin/mock
PYLINT=/usr/bin/pylint
PYTHON=/usr/bin/python3
PYTHON_SETUP=$(PYTHON) setup.py
RPM=/usr/bin/rpm
RPM_IMPORT=$(RPM) --import
RPMLINT=/usr/bin/rpmlint
SHELL=/bin/sh
SHELLCHECK=/usr/bin/shellcheck --shell=bash
SHELLCHECK_X=$(SHELLCHECK) -x
YAMLLINT=/usr/bin/yamllint

# Standard Makefile installation directories.
#
# The following are the standard GNU/RPM values which are defined in
# /usr/lib/rpm/macros. See
# http://www.gnu.org/software/make/manual/html_node/Directory-Variables.html
# for more information.
prefix=/usr
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin
datadir=$(prefix)/share
includedir=$(prefix)/include
infodir=$(prefix)/info
libdir=$(exec_prefix)/lib
libexecdir=$(exec_prefix)/libexec
localstatedir=$(prefix)/var
mandir=$(prefix)/man
rpmconfigdir=$(libdir)/rpm
sbindir=$(exec_prefix)/sbin
sharedstatedir=$(prefix)/com
sysconfdir=$(prefix)/etc

# Fedora installation directory overrides.
#
# We override some of the previous GNU/RPM default values with those
# values suiteable for a Fedora/RedHat/CentOS linux system, as defined
# in /usr/lib/rpm/redhat/macros.
#
# This section can be put into a separate file and included here if we
# want to create a more multi-platform Makefile system.
sysconfdir=/etc
defaultdocdir=/usr/share/doc
infodir=/usr/share/info
localstatedir=/var
mandir=/usr/share/man
sharedstatedir=$(localstatedir)/lib

# Makefile parameter variables
name:=morphcv
version=$(shell awk '/Version:/ { print $$2 }' $(name).spec)
release=$(subst %{?dist},,$(shell awk '/Release:/ { print $$2 }' $(name).spec))
dist_name:=$(name)-$(version)
dist_tree-ish:=$(version)
tarball:=$(dist_name).tar.gz
fedora_release:=29
fedora_dist:=$(release).fc$(fedora_release)
fedora_rpm_stem:=$(dist_name)-$(fedora_dist)
fedora_srpm:=$(fedora_rpm_stem).src.rpm
fedora_rpm:=$(fedora_rpm_stem).noarch.rpm
fedora_mock_root:=fedora-$(fedora_release)-x86_64
mock_resultdir=.
testdestdir:=testdestdir
git_hooks_tgt_files:=$(patsubst hooks/%,.git/hooks/%,$(wildcard hooks/*))
development_requirements:=git mock rpmlint ShellCheck yamllint 
requirements:=libreoffice-core libxml2 libxslt texlive-collection-latexrecommended texlive-datetime2 texlive-pdfcomment texlive-soulutf8 utf2rtf

.PHONY: all
all: xslt/morphcv-css-theme-classic.xml xslt/morphcv-css-theme-modern.xml
	$(info all:)

.PHONY: lint
lint:
	$(info precheck:)
	$(SHELLCHECK) *.bash
	$(RPMLINT) $(name).spec
	$(YAMLLINT) .gitlab-ci.yml

xslt/morphcv-css-theme-classic.xml: css/classic.css
	echo '<style type="text/css" title="Morphcv Theme Classic">' > $@
	cat $< >> $@
	echo '</style>' >> $@

xslt/morphcv-css-theme-modern.xml: css/modern.css
	echo '<style type="text/css" title="Morphcv Theme Modern">' > $@
	cat $< >> $@
	echo '</style>' >> $@

.PHONY: testinstall
testinstall: | testdestdir install installcheck

testdestdir:
	mkdir $(testdestdir)
	$(eval DESTDIR:=$(testdestdir))

.PHONY: testinstall
testinstall: | testdestdir install installcheck

.PHONY: install
install: all
	$(info install:)
	$(INSTALL_PROGRAM) morphcv-to-docx.bash $(DESTDIR)$(bindir)/morphcv-to-docx
	$(INSTALL_PROGRAM) morphcv-to-latex.bash $(DESTDIR)$(bindir)/morphcv-to-latex
	$(INSTALL_PROGRAM) morphcv-to-pdf.bash $(DESTDIR)$(bindir)/morphcv-to-pdf
	$(INSTALL_PROGRAM) morphcv-to-rtf.bash $(DESTDIR)$(bindir)/morphcv-to-rtf
	$(INSTALL_PROGRAM) morphcv-to-xhtml5.bash $(DESTDIR)$(bindir)/morphcv-to-xhtml5
	$(INSTALL_PROGRAM) morphcv-validate.bash $(DESTDIR)$(bindir)/morphcv-validate
	$(INSTALL_DIR) $(DESTDIR)$(datadir)/morphcv/css
	$(INSTALL_DATA) css/*.css $(DESTDIR)$(datadir)/morphcv/css
	$(INSTALL_DIR) $(DESTDIR)$(defaultdocdir)/morphcv/europass
	$(INSTALL_DATA) doc/europass/* $(DESTDIR)$(defaultdocdir)/morphcv/europass
	$(INSTALL_DIR) $(DESTDIR)$(defaultdocdir)/morphcv/latex
	$(INSTALL_DATA) doc/latex/* $(DESTDIR)$(defaultdocdir)/morphcv/latex
	$(INSTALL_DIR) $(DESTDIR)$(defaultdocdir)/morphcv/rtf
	$(INSTALL_DATA) doc/rtf/* $(DESTDIR)$(defaultdocdir)/morphcv/rtf
	$(INSTALL_DIR) $(DESTDIR)$(datadir)/morphcv/xsd/morphcv
	$(INSTALL_DATA) xsd/morphcv/*.xsd $(DESTDIR)$(datadir)/morphcv/xsd/morphcv
	$(INSTALL_DIR) $(DESTDIR)$(datadir)/morphcv/xsd/XHTML5-XML-Schema-master
	$(INSTALL_DATA) xsd/XHTML5-XML-Schema-master/*.xsd $(DESTDIR)$(datadir)/morphcv/xsd/XHTML5-XML-Schema-master
	$(INSTALL_DIR) $(DESTDIR)$(datadir)/morphcv/xsd/europass-xml-schema-definition-v3.3.0/{imported,included}
	$(INSTALL_DATA) xsd/europass-xml-schema-definition-v3.3.0/*.xsd $(DESTDIR)$(datadir)/morphcv/xsd/europass-xml-schema-definition-v3.3.0
	$(INSTALL_DATA) xsd/europass-xml-schema-definition-v3.3.0/imported/*.xsd $(DESTDIR)$(datadir)/morphcv/xsd/europass-xml-schema-definition-v3.3.0/imported
	$(INSTALL_DATA) xsd/europass-xml-schema-definition-v3.3.0/included/*.xsd $(DESTDIR)$(datadir)/morphcv/xsd/europass-xml-schema-definition-v3.3.0/included
	$(INSTALL_DIR) $(DESTDIR)$(datadir)/morphcv/xslt
	$(INSTALL_DATA) xslt/*.xslt xslt/*.xml $(DESTDIR)$(datadir)/morphcv/xslt
	$(INSTALL_DIR) $(DESTDIR)$(datadir)/texlive/texmf-dist/tex/latex/morphcv
	$(INSTALL_DATA) latex/*.cls $(DESTDIR)$(datadir)/texlive/texmf-dist/tex/latex/morphcv

.PHONY: installcheck
installcheck:
	$(info installcheck:)
	test -x $(DESTDIR)$(bindir)/morphcv-to-latex
	test -x $(DESTDIR)$(bindir)/morphcv-to-pdf
	test -x $(DESTDIR)$(bindir)/morphcv-to-rtf
	test -x $(DESTDIR)$(bindir)/morphcv-to-xhtml5
	test -x $(DESTDIR)$(bindir)/morphcv-validate
	test -r $(DESTDIR)$(datadir)/texlive/texmf-dist/tex/latex/morphcv/morphcv.cls	

.PHONY: postinstall
postinstall:
	$(info postinstall:)
	texhash

.PHONY: uninstall
uninstall:
	$(info uninstall:)
	rm -f $(DESTDIR)$(bindir)/morphcv-*
	rm -rf $(DESTDIR)$(datadir)/morphcv
	rm -rf $(DESTDIR)$(defaultdocdir)/morphcv
	rm -rf $(DESTDIR)$(datadir)/texlive/texmf-dist/tex/latex/morphcv	

# Generate a distribution tarball
.PHONY: dist
dist: $(tarball)

$(tarball):
	$(GIT) archive --format=tar --prefix=$(dist_name)/ $(dist_tree-ish) | $(GZIP) > $(tarball)

.PHONY: srpm
srpm: $(fedora_srpm)

$(fedora_srpm): $(tarball)
	$(MOCK) --root=$(fedora_mock_root) --resultdir=$(mock_resultdir) --buildsrpm \
		--spec $(name).spec --sources $(tarball)

.PHONY: rpm
rpm: $(fedora_srpm)
	$(MOCK) --root=$(fedora_mock_root) --resultdir=$(mock_resultdir) --rebuild $(fedora_srpm)

.PHONY: installgithooks
installgithooks: $(git_hooks_tgt_files)
	$(info All git_hook have been installed in .git/hooks directory.)

$(git_hooks_tgt_files): hooks/*
	cp $< $@

.PHONY: requirements
requirements:
	sudo dnf copr enable danieljrmay/morphcv 
	sudo dnf install $(development_requirements) $(requirements)

.PHONY: clean
clean:
	$(info clean:)
	rm -rf build
	rm -f *.rpm *.tar.gz *.log

.PHONY: distclean
distclean: clean
	$(info distclean:)
	find . -name '*~' -delete
	rm -rf testdestdir
	rm -f xslt/morphcv-css-theme-classic.xml xslt/morphcv-css-theme-modern.xml

.PHONY: help
help:
	$(info help:)
	$(info Usage: make TARGET [VAR1=VALUE VAR2=VALUE])
	$(info )
	$(info Targets:)
	$(info   all             The default target, redirects to build.)
	$(info   lint            Lint the source files.)
	$(info   testinstall     Perform a test installation.)
	$(info   install         Install.)
	$(info   installcheck    Post-installation check of all installed files.)
	$(info   postinstall     Runs texhash, which indexes TeX files, shoule be run after install as required.)
	$(info   uninstall       Uninstall.)
	$(info   installgithooks Install the git hooks in the local repository.)
	$(info   dist            Create a distribution tarball.)
	$(info   srpm            Create a SRPM.)
	$(info   rpm             Create a RPM.)
	$(info   requirements    Install development and runtime requirements for this package.)
	$(info   clean           Clean up all generated binary files.)
	$(info   distclean       Clean up all generated files.)
	$(info   help            Display this help message.)
	$(info   printvars       Print variable values (useful for debugging).)
	$(info   printmakevars   Print the Make variable values (useful for debugging).)
	$(info )

.PHONY: printvars
printvars:
	$(info printvars:)
	$(info project_name=$(project_name))
	$(info project_version=$(project_version))
	$(info INSTALL=$(INSTALL))
	$(info INSTALL_DATA=$(INSTALL_DATA))
	$(info INSTALL_DIR=$(INSTALL_DIR))
	$(info INSTALL_PROGRAM=$(INSTALL_PROGRAM))
	$(info MOCKROOT=$(MOCKROOT))
	$(info MOCKRESULTDIR=$(MOCKRESULTDIR))
	$(info prefix=$(prefix))
	$(info exec_prefix=$(exec_prefix))
	$(info bindir=$(bindir))
	$(info datadir=$(datadir))
	$(info includedir=$(includedir))
	$(info infodir=$(infodir))
	$(info libdir=$(libdir))
	$(info libexecdir=$(libexecdir))
	$(info localstatedir=$(localstatedir))
	$(info mandir=$(mandir))
	$(info rpmconfigdir=$(rpmconfigdir))
	$(info sbindir=$(sbindir))
	$(info sharedstatedir=$(sharedstatedir))
	$(info sysconfdir=$(sysconfdir))

.PHONY: printmakevars
printmakevars:
	$(info printmakevars:)
	$(info $(.VARIABLES))
