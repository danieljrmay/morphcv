%%
%% morphcv-modern.cls
%%
%% This LaTeX class file provides the morphcv documentclass.
%%
%% It builds on top of the article documentclass
%%
%% Useful documentation:
%% www.sharelatex.com/blog/2013/06/28/how-to-write-a-latex-class-file-and-design-your-own-cv.html

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{morphcv-modern}[22/11/2017 custom CV document class]
\LoadClass{article}
\pagestyle{empty}
\RequirePackage[a4paper,ignoreall,scale=0.86]{geometry}
\RequirePackage{titlesec}
\RequirePackage{multicol}
\RequirePackage{enumitem}
\RequirePackage{hyperref}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

\raggedbottom

\titleformat{\section}{\Large\raggedright}{}{0mm}{}[\titlerule]
\titleformat{\subsection}{\large\raggedright}{}{0mm}{}
\titleformat{\subsubsection}{\normalsize\raggedright}{}{0mm}{}[\vspace{-0.5em}]

\newcommand{\name}[1]{
  \centerline{
    \Huge{#1}
  }
  \vspace{0.67em}
}

\newcommand{\role}[1]{
  \centerline{
    \Large{#1}
  }
}

%\newcommand{\contact}[4]{
%  \centerline{#1\quad\textbullet\quad#2}
%  \centerline{#3\quad\textbullet\quad#4}
%}

\newcommand{\contact}[3]{
  \centerline{#1\quad\textbullet\quad#2}
  \centerline{#3}
}

\newcommand{\datedsection}[2]{
  \section[#1]{#1 \hfill #2}
}

\newcommand{\datedsubsection}[2]{
  \subsection[#1]{#1 \hfill \normalsize #2}
}

\newcommand{\datedsubsubsection}[2]{
  \subsubsection[#1]{#1 \hfill \normalsize #2}
}

% Make all fonts sans-serif by default. We also use avant which is a
% round art-deco style sans-serif font.
\usepackage{avant}
\renewcommand{\familydefault}{\sfdefault}
