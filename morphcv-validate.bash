#!/usr/bin/bash
# morphcv-validate.bash
#
# Author: Daniel J. R. May <daniel.may@kada-media.com>
# Version: 0.2, 10 September 2020
# Description: Validate a morphcv XML against the morphcv XML Schema.
#
# This file is part of morphcv.
# 
# morphcv is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# morphcv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with morphcv.  If not, see <http://www.gnu.org/licenses/>.

readonly morphcv_schema_path_local=xsd/morphcv/morphcv.xsd
readonly morphcv_schema_path_global=/usr/share/morphcv/xsd/morphcv/morphcv.xsd

function print_help {
    gettext -s 'mophcv-validate'
    gettext -s 'Author: Daniel J. R. May'
    gettext -s 'Version: 0.3, 3 December 2020'
    echo
    gettext -s 'Validates your morphcv source file.'
    echo
    print_usage
}

function print_usage {
    gettext -s 'Usage:'
    echo       '    morphcv-validate [-h|--help]'
    echo       '    morphcv-validate FILE'
    echo
}


# Entry point

# Procress the command line arguments
case "$1" in
    '-h' | '--help')
        print_help
        exit 0
        ;;
    *)
        if [[ -z "$1" ]]
        then
            gettext -s 'Syntax error: you must specify a morphcv XML file to validate.'
            print_usage
            exit 1
        elif [[ ! -e "$1" ]]
        then
            gettext -s 'File error: the file you specified does not exist, please check your spelling.'
            print_usage
            exit 2
        elif [[ ! -r "$1" ]]
        then
            gettext -s 'File error: the file you specified can not be read, please check its file permissions.'
            print_usage
            exit 3
        else
            # Morphcv source file has been specified, exists and is
            # readable, so we continue
            readonly input_path=$1
        fi
        ;;
esac

# Check xmllint is available
if (! command -v xmllint > /dev/null )
then
    gettext -s 'Requirements error: unable to find the xmllint command which is needed by this program.'
    gettext -s 'xmllint is part of libxml2, see http://xmlsoft.org/ for more details.'
    gettext -s 'You can typically install it with something like:'
    gettext -s -e "\tdnf install libxml2"
    exit 4
fi

# Check MORPHCV_MORPHCV_SCHEMA_PATH variables exists and contains a
# readable path
if [[ -v MORPHCV_MORPHCV_SCHEMA_PATH ]] && [[ -r $MORPHCV_MORPHCV_SCHEMA_PATH ]]
then
    gettext -s "Warning: Using the MORPHCV_MORPHCV_SCHEMA_PATH=$MORPHCV_MORPHCV_SCHEMA_PATH environment variable for the location of the morphcv.xsd file."
    gettext -s "If you wish to revert to the program defaults for the location of the morphcv.xsd file, then issue the following command:"
    gettext -s -e "\tunset MORPHCV_MORPHCV_SCHEMA_PATH"
    gettext -s "before running this program again."
    morphcv_schema_path=$MORPHCV_MORPHCV_SCHEMA_PATH    
elif [[ -v MORPHCV_MORPHCV_SCHEMA_PATH ]] && [[ ! -r $MORPHCV_MORPHCV_SCHEMA_PATH ]]
then
    gettext -s "Requirements error: unable to find morphcv.xsd at the custom location specified by MORPHCV_MORPHCV_SCHEMA_PATH=$MORPHCV_MORPHCV_SCHEMA_PATH environment variable."
    gettext -s "Correct this environment variable by issuing the command:"
    gettext -s -e "\texport MORPHCV_MORPHCV_SCHEMA_PATH=/path/to/morphcv.xsd"
    gettext -s "before running this program again."
    gettext -s "Alternativly, revert to the program defaults with:"
    gettext -s -e "\tunset MORPHCV_MORPHCV_SCHEMA_PATH"
    exit 5
elif [[ -r $morphcv_schema_path_local ]]
then
    morphcv_schema_path=$morphcv_schema_path_local
elif [[ -r $morphcv_schema_path_global ]]
then
    morphcv_schema_path=$morphcv_schema_path_global
else
    gettext -s "Requirements error: unable to find morphcv.xsd which is needed by this program."
    gettext -s "The file is not present in the following default locations:"
    gettext -s "$morphcv_schema_path_local"
    gettext -s "$morphcv_schema_path_global"
    gettext -s "You can get this program to use a custom location by issuing the command:"
    gettext -s -e "\texport MORPHCV_MORPHCV_SCHEMA_PATH=/path/to/morphcv.xsd"
    gettext -s "before running this program again."
    exit 6
fi

# Execute xmllint
xmllint --noout --schema "$morphcv_schema_path" "$input_path"
