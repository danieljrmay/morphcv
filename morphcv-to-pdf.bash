#!/usr/bin/bash
# morphcv-to-pdf.bash
#
# Author: Daniel J. R. May <daniel.may@kada-media.com>
# Version: 0.3, 10 September 2020
# Description: Convert a  morphcv XML to a PDF via LaTeX.
#
# This file is part of morphcv.
# 
# morphcv is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# morphcv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with morphcv.  If not, see <http://www.gnu.org/licenses/>.

readonly input_path=$1

intermediate_path="$(basename "$input_path" .xml).tex"
readonly intermediate_path

function print_usage {
    gettext -s 'Usage: morphcv-to-pdf FILE'
    gettext -s 'Where FILE is something like: path/to/my-morphcv-file.xml'
}

if [[ -z "$input_path" ]]
then
    gettext -s 'Syntax error: you must specify a morphcv XML file to be converted to PDF.'
    print_usage
    exit 1
fi

if [[ ! -e "$input_path" ]]
then
    gettext -s 'File error: the file you specified does not exist, please check your spelling.'
    print_usage
    exit 2
fi

if [[ ! -r "$input_path" ]]
then
    gettext -s 'File error: the file you specified can not be read, please check its file permissions.'
    print_usage
    exit 3
fi

if (! command -v morphcv-to-latex > /dev/null )
then
    gettext -s 'Requirements error: unable to find the morphcv-to-latex command which is needed by this program.'
    gettext -s 'morphcv-to-latex is part of the morphcv package.'
    exit 4
fi

if (! command -v pdflatex > /dev/null )
then
    gettext -s 'Requirements error: unable to find the pdflatex command which is needed by this program.'
    gettext -s 'pdflatex is part of TODO PACKAGE.'
    gettext -s 'You can typically install it with something like:'
    gettext -s -e "\tdnf install TODO"
    exit 4
fi

# We convert the morphcv XML file to latex and then run pdflatex on it
# twice to make sure all cross-refernces and contents are finalized.
morphcv-to-latex "$input_path" && \
    pdflatex "$intermediate_path" && \
    pdflatex "$intermediate_path"
