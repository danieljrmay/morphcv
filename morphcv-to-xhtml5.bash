#!/usr/bin/bash
# morphcv-to-xhtml5.bash
#
# Author: Daniel J. R. May <daniel.may@kada-media.com>
# Version: 0.1, 10 September 2020
# Description: Convert a  morphcv XML file to XHTML5.
#
# This file is part of morphcv.
# 
# morphcv is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# morphcv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with morphcv.  If not, see <http://www.gnu.org/licenses/>.

#############
# Variables #
#############

readonly MORPHCV_SCHEMA_PATH_LOCAL=xsd/morphcv/morphcv.xsd
readonly MORPHCV_SCHEMA_PATH_GLOBAL=/usr/share/morphcv/xsd/morphcv/morphcv.xsd
readonly MORPHCV_XSLT_PATH_LOCAL=xslt/morphcv-to-xhtml5.xslt
readonly MORPHCV_XSLT_PATH_GLOBAL=/usr/share/morphcv/xslt/morphcv-to-xhtml5.xslt

infile=${!#}
outfile=$(basename --suffix=xml "$infile")xhtml
standalone="false"
theme="classic"
verbose="false"


#############
# Functions #
#############

function print_usage {
    gettext -s 'Usage: morphcv-to-xhtml5 [-h|-s] [-o outfile] [-t theme] infile'
    gettext -s '    morphcv-to-xhtml5 -h'
    gettext -s '    morphcv-to-xhtml5 [-s] [-o outfile] [-t theme] FILE'
    gettext -s 'Where FILE is something like: path/to/my-morphcv-file.xml'
}


###############
# Entry point #
###############

# Parse the command line arguments
while getopts "ho:st:v" opt
do
    case "$opt" in
	h)
            print_usage
            exit 0
            ;;
	o)
	    outfile=$OPTARG
	    ;;
	s)
	    standalone="true"
            ;;
	t)
	    theme=$OPTARG
            ;;
	v)
	    verbose="true"
	    ;;
	*)
	    gettext -s "Syntax error: illegal command line option."
	    print_usage
	    exit 1    	    
    esac
done

# Check morphcv XML schemas exist and select whether we are using a
# local or global version
if [[ -r $MORPHCV_SCHEMA_PATH_LOCAL ]]
then
    morphcv_schema=$MORPHCV_SCHEMA_PATH_LOCAL
elif [[ -r $MORPHCV_SCHEMA_PATH_GLOBAL ]]
then
    morphcv_schema=$MORPHCV_SCHEMA_PATH_GLOBAL
else
    gettext -s "Requirements error: unable to locate a morphcv schema."
    exit 4
fi

# Check morphcv-to-xhtml5 XSLT file exists and select whether we are
# using a local or global version
if [[ -r $MORPHCV_XSLT_PATH_LOCAL ]]
then
    morphcv_xslt=$MORPHCV_XSLT_PATH_LOCAL
elif [[ -r $MORPHCV_XSLT_PATH_GLOBAL ]]
then
    morphcv_xslt=$MORPHCV_XSLT_PATH_GLOBAL
else
    gettext -s "Requirements error: unable to locate a morphcv-to-xhtml5 XSLT file."
    exit 4
fi


# Display the variable values if operating in verbose mode
if [[ $verbose == "true" ]]
then
    echo "infile=$infile"
    echo "morphcv_schema=$morphcv_schema"
    echo "morphcv_xslt=$morphcv_xslt"
    echo "outfile=$outfile"
    echo "standalone=$standalone"
    echo "theme=$theme"
    echo "verbose=$verbose"
fi

# Validate that an input file has been provided
if [[ -z "$infile" ]]
then
    gettext -s 'Syntax error: you must specify a morphcv XML file.'
    print_usage
    exit 1
fi

# Validate that the provided input file exists
if [[ ! -e "$infile" ]]
then
    gettext -s 'File error: the file you specified does not exist, please check your spelling.'
    print_usage
    exit 2
fi

# Validate that the provided input file is readable
if [[ ! -r "$infile" ]]
then
    gettext -s 'File error: the file you specified can not be read, please check its file permissions.'
    print_usage
    exit 3
fi

# Check that the xmllint command is available
if (! command -v xmllint > /dev/null )
then
    gettext -s 'Requirements error: unable to find the xmllint command which is needed by this program.'
    gettext -s 'xmllint is part of TODO PACKAGE.'
    gettext -s 'You can typically install it with something like:'
    gettext -s -e "\tdnf install TODO"
    exit 4
fi

# Check that the xsltproc command is available
if (! command -v xsltproc > /dev/null )
then
    gettext -s 'Requirements error: unable to find the xsltproc command which is needed by this program.'
    gettext -s 'xsltproc is part of TODO PACKAGE.'
    gettext -s 'You can typically install it with something like:'
    gettext -s -e "\tdnf install TODO"
    exit 4
fi

# Check the input morphcv XML file against the morphcv XML Schema and
# if it validates we proceed to convert it to XHTML5 via XSLT using
# the morphcv-to-xhtml5.xslt file.
xmllint -schema "$morphcv_schema" "$infile" --noout && \
    xsltproc --stringparam stand-alone "$standalone" --stringparam theme "$theme" --output "$outfile" "$morphcv_xslt" "$infile"
